import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from "../../node_modules/observable/";

const base = '/' + environment.proxy;
const clientes = base + '/api/v1/clientes';
const cliente = base + '/api/v1/cliente';

class Persona {
  rut: string;
  fecha: Date;
  nombre: String;
  tipoCliente: String;
}

class addPersona {
  rut: string;
  nombre: String;
  tipoCliente: String;
}
@Injectable({
  providedIn: 'root'

})
export class ApiService {
  personas$: Array<Persona>[] = new Array();
  responseAddPersona = new Persona();

  constructor(private http: HttpClient) { }

  async getClientes() {
    return await this.http.get(clientes).toPromise();
  }

  async addCliente(persona: addPersona): Observable<Persona> {
    return await this.http.post(cliente, persona).toPromise();
  }

  async updateCliente(persona: addPersona): Observable<Persona> {
    await this.http.put<addPersona>(cliente, persona).toPromise();
  }

  async deleteCliente(rut: String): Observable<any> {
    return await this.http.delete(cliente + "/" + rut).toPromise();
  }

}
