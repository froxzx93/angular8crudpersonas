import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { PersonaComponent } from './persona/persona.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { PaletaComponent } from './paleta/paleta.component'

@NgModule({
  declarations: [NavbarComponent, PersonaComponent, PaletaComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    NavbarComponent,
    PersonaComponent,
    PaletaComponent
  ]
})
export class ComponentsModule { }
