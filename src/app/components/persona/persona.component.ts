import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../../api.service';

class addPersona {
  rut: string;
  nombre: String;
  tipoCliente: String;
}
class Persona {
  rut: string;
  fecha: Date;
  nombre: String;
  tipoCliente: String;
}
class notificacion {
  rut: string;
  accion: string
}

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})

export class PersonaComponent implements OnInit {
  @Output()
  mensajeNotificacion = new EventEmitter<notificacion>();
  personas$: Array<Persona> = [];
  indice: number = null;
  persona = new addPersona();
  rutDelete = "";
  rutEdit = "";
  mensaje = new notificacion();
  constructor(private api: ApiService) {
  }

  ngOnInit() {
    this.getAll();
  }
  async enviarNotificacion(accion: string, rut: string) {
    this.mensaje.accion = accion;
    this.mensaje.rut = rut;
    this.mensajeNotificacion.emit(this.mensaje);
  }

  async getAll() {
    this.personas$.splice(0, this.personas$.length);
    const response = await this.api.getClientes();
    response["data"].forEach(element => {
      return this.personas$.push(element);
    });

  }
  async add() {
    this.persona.tipoCliente = "Persona";
    const response = await this.api.addCliente(this.persona);
    await this.getAll();
    await this.enviarNotificacion("Agregar", this.persona.rut);
  }

  async update() {
    this.persona.rut = this.rutEdit;
    this.persona.tipoCliente = "Persona";
    await this.api.updateCliente(this.persona);
    await this.getAll();
    await this.enviarNotificacion("Editar", this.rutEdit);
  }

  async delete() {
    const data = await this.api.deleteCliente(this.rutDelete);
    await this.getAll();
    await this.enviarNotificacion("Eliminar", this.rutDelete);
  }

}
