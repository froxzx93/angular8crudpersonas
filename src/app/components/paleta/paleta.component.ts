import { Component, OnInit, Input, NgModule } from '@angular/core';

class Notificacion {
  rut: string;
  accion: string
}

@Component({
  selector: 'app-paleta',
  templateUrl: './paleta.component.html',
  styleUrls: ['./paleta.component.scss']
})
export class PaletaComponent implements OnInit {

  @Input() mensajeNotificacion: Notificacion = new Notificacion();

  constructor() {
  }

  closeNotificacion() {
    this.mensajeNotificacion.rut = null;
  }

  selectClass() {
    switch (this.mensajeNotificacion.accion) {
      case "Agregar":
        return "notify-add";
      case "Eliminar":
        return "notify-delete";
      case "Editar":
        return "notify-put";
    }
  }
  ngOnInit() {
  }

}
