import { Component, OnInit } from '@angular/core';

class Notificacion {
  rut: string;
  accion:string
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  mensaje: Notificacion = new Notificacion();;
  
  constructor() { }

  ngOnInit() {
  }

}
