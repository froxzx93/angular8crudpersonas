import { environment } from '../environments/environment';
//CONST BASE
const base = '/' + environment.proxy;

//URL endpoints
const clientes = base + '/api/v1/clientes';
const cliente = base + '/api/v1/cliente';

export default {
    cliente,
    clientes
} 